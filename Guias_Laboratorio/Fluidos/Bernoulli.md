# Ecuación de Bernoulli

La idea de este experimento es verificar la ecuación de Bernoulli, utilizando el experimento que se muestra en la figura.

![Fig. 1](https://gitlab.com/pcremades/labfisica_1/raw/master/Guias_Laboratorio/Fluidos/Bernoulli.jpg)

La ecuación de Bernoulli para los puntos $`p_1`$ y $`p_2`$ es:

```math
 \frac 1 2 . \rho . {v_1}^2 + \rho . g . h_1 + P_1 = \frac 1 2 . \rho . {v_2}^2 + \rho . g . h_2 + P_2
```

Dado que la sección del tanque es mucho mayor que la del orificio de salida, podemos suponer que la velocidad $`v_1`$ es cero.
Por otro lado, las presiones $`P_1`$ y $`P_2`$ son iguales a la presión atmosférica. Finalmente, llamando $`H = h_1 - h_2`$ la
ecuación se resume a:

```math
 \frac 1 2 . \rho . {v_2}^2  = \rho . g . H
```
Por lo tanto, la velocidad con la que sale el agua es:

```math
 {v_2}  = \sqrt{2 . g . H}
```

Si llamamos $`\Delta H`$ al error asociado a la medida de la altura de la columna de agua, entonces el error en la velocidad será:
```math
 \Delta v_2 = \sqrt{ {\frac {\partial v_2} {\partial H}}^2 . {\Delta H}^2 }
 \newline
 \frac {\partial v_2} {\partial H} = \sqrt{\frac g {2.H}}
```

Podemos también calcular la velocidad de salida del agua a partir de consideraciones cinemáticas.

En el eje $`x`$ el movimiento es MRU:

```math
x = v_2 . t
```

En el eje $`y`$ el movimiento es MRUV:

```math
v_f = g.t => t= v_f /g
\newline
v_f^2 = 2.g.h_2
\newline
t = \sqrt{\frac {2.h_2} g}
```
reemplazando el tiempo en la ecuación cinemática para el eje $`x`$:

```math
v_2 = x . \sqrt{\frac g {2.h_2}}
```

El error en este caso viene dado por:
```math
\Delta v = \sqrt{ {\frac {\partial v_2} {\partial h_2}}^2 . {\Delta h_2}^2 + {\frac {\partial v_2} {\partial x}}^2 . {\Delta x}^2}
\newline
 \frac {\partial v_2} {\partial h_2} = - \frac x {2.h_2} . \sqrt{ \frac g {2.h_2}}
\newline
 \frac {\partial v_2} {\partial x} = \sqrt {\frac g {2.h_2}}
```
