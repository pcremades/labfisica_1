

bearingExtDiam = 22.1;
bearingIntDiam = 7.9;
bearingWidth = 7;
cunaWidth_1 = 4;
cunaWidth_2 = 11;
cunaHeight = 3.8;
wall = 2;

//cuña
module cuna(){
  hull(){
    cube([cunaWidth_1, bearingExtDiam+2*wall, 0.01], center=true);
    translate([0, 0, cunaHeight]) cube([cunaWidth_2, bearingExtDiam+2*wall, 0.01], center=true);
  }
}

//bearing holder
module bearingHolder(){
  difference(){
    cube([cunaWidth_2, bearingExtDiam+2*wall, bearingExtDiam+wall], center=true);
    union(){
      translate([cunaWidth_2/2-bearingWidth/2, 0, (bearingExtDiam+wall)/2-bearingExtDiam/2-wall/2]) rotate([0, 90, 0]) cylinder(d=bearingExtDiam, h=bearingWidth, center=true);
      translate([cunaWidth_2/2-bearingWidth/2, 0, (bearingExtDiam+wall)/2-bearingExtDiam/2-wall/2]) rotate([0, 90, 0]) cylinder(d=bearingExtDiam/2, h=bearingWidth*3, center=true);
    }
  }
}

translate([0, 0, bearingExtDiam/2+cunaHeight+wall/2])bearingHolder();
cuna();
