IR_sensor_width = 7.5;
IR_sensor_length = 7.5;
Arduino_width = 19;
Arduino_length = 44;
usb_width = 8;
wall = 3;
case_height = 9;
IR_board_width = 17;
screw_diam = 3;

$fn = 20;

module screw_hole(){
  difference(){
    union(){
      cylinder(d=screw_diam+wall*2, h=wall, center=true);
      translate([screw_diam, 0, 0]) cube([screw_diam+wall*2, screw_diam+wall*2, wall], center=true);
    }
    cylinder(d=screw_diam, h=wall*2, center=true);
  }
}

difference(){
  minkowski(){
    cube([Arduino_length, Arduino_width, case_height], center=true);
    cylinder(d=wall, h=wall/2);
    }
  translate([0, 0, -case_height*0]) cube([Arduino_length, Arduino_width, case_height], center=true);
  translate([Arduino_length/2-5.8, 1.4, 0]) cube([IR_sensor_length, IR_sensor_width, 1000], center=true);
  translate([Arduino_length/2, 0, case_height/2-wall-1]) cube([20, IR_board_width, 6.5], center=true); //IR sensor board cut
  #translate([-Arduino_length/2, 0, 0]) cube([10, usb_width, 6.3], center=true);
}

translate([0, -(4*screw_diam+2*wall), -(case_height/2-wall/2)]) rotate([0, 0, 90]) screw_hole();
translate([0, (4*screw_diam+2*wall), -(case_height/2-wall/2)]) rotate([0, 0, 270]) screw_hole();
