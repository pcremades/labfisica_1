bearing_diam = 32;
nut_diam = 7;
bearing_h = 4.1;
nut_h = 2.3;
wall = 2;

$fn=50;

module bearing_nuts_holder(){
  cylinder(d=bearing_diam, h=bearing_h, center=true);
  translate([0, 0, bearing_h/2]) cylinder(d=nut_diam, h=nut_h, center=true, $fn=6);
  translate([0, 0, -bearing_h/2])cylinder(d=nut_diam, h=nut_h, center=true, $fn=6);
}

module tool(){
  difference(){
    hull(){
      translate([nut_diam/2, 0, 0]) cylinder(d=nut_diam+wall*2, h=bearing_h+1*nut_h, center=true);
      translate([bearing_diam, 0, 0]) cylinder(d=nut_diam+wall*2, h=bearing_h+1*nut_h, center=true);
    }
    bearing_nuts_holder();
  }
}

difference(){
  tool();
  translate([0, 0, 25]) cube([150, 150, 50], center=true);
}
