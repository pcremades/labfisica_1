/* Copyright 2015 Pablo Cremades //<>// //<>// //<>//
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**************************************************************************************
 * Autor: Pablo Cremades
 * Fecha: 03/08/2016
 * e-mail: pablocremades@gmail.com
 * Descripción: Aplicación para medir velocidad de los sensores INGKA. Para iniciar
 * una medición, conecte el equipo de adquisición de datos a la PC y presione el
 * botón "Iniciar". Cuando termine, presione el botón "Detener".
 *
 * Change Log:
 * - Borrar el código para inicializar la placa con comandos de teclado!!!!
 * 04/08/2016: Agregué slider para el definir el ancho de la bandera.
 * - 11/08/2016: Esta versión funciona por interrupciones. Hay Sensores
 *  de INGKA que por alguna razón no funcionan!!!!!!!!!!
 */

import g4p_controls.*;
import processing.serial.*;

String[] serialPorts;
Serial port;
GButton Iniciar, Detener, Calibrar;
String inString = "";
String[] list;
float[] tiempo = new float[2];
int[] sensorStatus = {0, 0};
float[] sensorSpeed = new float[2];
float calibra;

void setup() {
  size(400, 300);
  serialPorts = Serial.list(); //Get the list of tty interfaces
  for ( int i=0; i<serialPorts.length; i++) { //Search for ttyACM*
    if ( serialPorts[i].contains("ttyACM") || serialPorts[i].contains("ttyUSB") ) {  //If found, try to open port.
      println(serialPorts[i]);
      try {
        port = new Serial(this, serialPorts[i], 115200);
        port.bufferUntil(10);
      }
      catch(Exception e) {
      }
    }
  }

  //Create the buttons.
  Iniciar = new GButton(this, 20, 20, 100, 30, "Iniciar");
  Detener = new GButton(this, 150, 20, 100, 30, "Detener");
  Calibrar = new GButton(this, 300, 20, 100, 30, "Calibrar");
}

float force;
void draw() {
  background(255);
  fill(0);
  if ( port == null ) {  //If failed to open port, print errMsg and exit.
    println("Equipo desconectado. Conéctelo e intente de nuevo.");
    exit();
  }

  //Draw the texts.
  text( "Fuerza ", 20, 100);
  text( str(force), 20,150);
  text( "[N]", 70, 150);
  //If there is a string available on the port, parse it.
  //Strings not begining with # are data from SAD.
  if (inString.length() > 5 && inString.charAt(0) != '#') {
    print(inString);
    list = split(inString, "\t"); //Split the string.
    int sensor = Integer.parseInt(list[0]) - 2; //Los sensores digitales son 2 y 3. Restamos 2 para usar de index (0 y 1).
    if ( sensor == 3 || sensor == 1 ) {
      float status = Float.parseFloat(list[2].trim());  //Status is in the 3rd substring
      force = round((status - calibra)*100)/100.0;
      if ( status != sensorStatus[sensor] ) {  //If status for any sensor changed from last time...
        if (status == 1) //status=1 means sensor is blocked. Start counting time.
          tiempo[sensor] =  Float.parseFloat(list[1]);
        else {  //Sensor is no longer blocked. Determine the time that has passed.
          tiempo[sensor] =  Float.parseFloat(list[1]) - tiempo[sensor];
        }
      }
    }
    inString = ""; //Empty the string.
  }
}

//Read the incoming data from the port.
void serialEvent(Serial port) { 
  inString = port.readString();
}

//Buttons event handler.
void handleButtonEvents(GButton button, GEvent event) {
  if (button == Iniciar && event == GEvent.CLICKED) {
    port.write("#0001");  //Inicio.
    delay(100);
    port.write("#0031 462815232 1943863296 831782912 1421148160 ");  //Código de autenticación
    delay(100);
    port.write("#0033 239589820 3486795892 3188765557 2136465651 ");  //Código de autenticación
    delay(100);
    port.write("#0005,3,11,11,7,1000,100,0,0,0,0,0,0,0,0");
    delay(100);
    port.write("#0007");
     delay(100);
  } else if (button == Detener && event == GEvent.CLICKED) {
    port.write("#0021"); //Close comunication.
    exit(); //Exit the app.
  } else if (button == Calibrar && event == GEvent.CLICKED) {
    calibra = force;
  }
}
