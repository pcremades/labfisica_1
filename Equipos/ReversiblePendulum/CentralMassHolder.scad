$fs = 0.4;
$fa = 1;

include<threads.scad>   


height = 60;
extern_diam = 19.3;
//intern_diam = 13; //Para varilla roscada de 1/2
intern_diam = 10; //Para barra lisa
nuts_height = 57;

bolt_diam = 4.9;
bolt_position = (height/2 - nuts_height/2 - 4);

difference(){
  cylinder(d=extern_diam, h=height, center=true);
  cylinder(d=intern_diam, h=height*2, center=true);
  #translate([0, -(extern_diam+intern_diam)/1.9, nuts_height/2 + bolt_position]) rotate([-90, 0, 0]) MetricBolt(4, 8);
  #translate([0, -(extern_diam+intern_diam)/1.9, -(nuts_height/2 + bolt_position)]) rotate([-90, 0, 0]) MetricBolt(4, 8);
}
