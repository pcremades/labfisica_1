$fs = 0.4;
$fa = 1;

include<threads.scad>



bar_diam = 10;
/*

Descripción: Pivotes para la barra lisa de 10mm
*/

module fix_bolt(){
  bolt_diam = 4.9;
    
  cylinder(d1=bolt_diam, d2=bolt_diam+0.3, h=5, center=true);
  #rotate([180, 0, 0]) MetricBolt(5, 10);
}

module knife_bolts(){
  bolt_diam = 3.2;
  head_diam = 7;
  head_height = 3.5;

  cylinder(d2=head_diam, d1=bolt_diam, h=head_height);
  rotate([180, 0, 0]) cylinder(d=bolt_diam, h=100);
}

//knife_bolts();


difference(){
  cube([55, bar_diam*2.5, 10], center=true);
  cylinder(d=bar_diam, h=100, center=true);
  translate([-bar_diam*2, 0, 2]) knife_bolts();
  translate([bar_diam*2, 0, 2]) knife_bolts();
  translate([0, -bar_diam+28, 0]) rotate([-90, 0, 0]) fix_bolt();
  rotate([180, 0, 0]) translate([0, -bar_diam+28, 0]) rotate([-90, 0, 0]) fix_bolt();
}
