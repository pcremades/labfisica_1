$fs = 0.4;
$fa = 1;

intern_diam = 12;
extern_diam = 16;
height = 10;

difference(){
  cylinder(d=extern_diam, h=height);
  cylinder(d=intern_diam, h=height/2);
}

translate([0, 0, height-0.5]) cylinder(d1=8, d2=3, h=30);
