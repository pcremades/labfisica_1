$fn = 50;

threadDiam = 25;
nutDiam = 38.2;
nutHeight = 15;
holderDiam = 50;

difference(){
  cylinder(h=nutHeight, d=holderDiam);
  translate([0,0,5]) cylinder(h=nutHeight, d=nutDiam, $fn=6);
  cylinder(h=nutHeight, d=threadDiam);
}
