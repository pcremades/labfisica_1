$fs=0.4;
$fa=1;

difference(){
  union(){
    cube([80, 20, 20]);
    translate([0, 20, 0]) cube([80, 10, 40]);
  }
  translate([15, 0, 0]) cylinder(r=8, h=100, center=true);
  translate([40, 0, 0]) cylinder(r=5, h=100, center=true);
  translate([65, 0, 0]) cylinder(r=8, h=100, center=true);
}
